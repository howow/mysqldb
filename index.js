'use strict'

const mysqlImp1 = require('promise-mysql')
const mysqlImp2 = require('mysql2/promise')

const hash = require('js-hash-code')
const LruCache = require('lru-cache')
const keyStore = new LruCache(20)

// mysql2 not support insecureAuth
const getMysql = (config) => (config.insecureAuth ? mysqlImp1 : mysqlImp2).createPool(config)
const getFormat = (config) => (config.insecureAuth ? mysqlImp1 : require('mysql2')).format

const AsyncCache = require('async-cache')
const proxy = new AsyncCache({
  'max': 10,
  'maxAge': 1000 * 60 * 60 * 4, // cache 4hr
  'load': (key, cbk) => cbk(null, getMysql(keyStore.get(key))),
  'dispose': (key, db) => db && db.end()
})

const get = require('util').promisify(proxy.get.bind(proxy))

const delay = ms => new Promise(resolve => setTimeout(resolve, ms))

// patch diff promise-mysql, mysql2/promise
// promise-mysql not has `execute`
const warpConnection = conn => {
  if (!(conn.execute || typeof conn.execute === 'function')) {
    conn.execute = conn.query
  }
  return conn
}

/**
 * use config and get a db object include following function
 * - getConnection: return a connection.
 * - query: execute a sql statment in a standalone connection.
 * - close: close db.
 * @example
 *
 * var db = mysql.connect(config);
 * var conn = db.getConnection();
 * var [rows] = await conn.execute('select 1');
 * or
 * var [rows] = await db.query('select 1');
 * @param  {object} config mysql pool options.
 * @return {object}        a object include getConnection, query and close function.
 */
const connect = (config) => {
  let key = hash(config)
  if (!keyStore.has(key)) keyStore.set(key, config)

  let getConnection = () => get(key).then(db =>
    db.getConnection().then(warpConnection)
      // .then(conn => conn.ping().then(() => conn))
      .catch(e => {
        console.log('>> ping dump >>\n', e, '\n===: ping :===')
        return get(key).then(db => db.getConnection())
      })
  )

  // let close = (cbk) => get(key).then(db => {
  //   console.log('fff', db.pool._closed)
  //   return db.end().then(() => {
  //     console.log('ggg', db.pool._closed)
  //     // db.pool._closed = false
  //   })
  // })
  let close = () => proxy.del(key)

  // let destroy = (conn) => {
  //   if (!conn) return
  //   setTimeout(() => {
  //     conn && conn.destroy()
  //   }, 30000)
  // }

  let query = (...args) => getConnection().then(conn =>
    conn.query(...args).catch(e => {
      console.log('\n\n>> mysql query dump >>\n', e,
        '\n===: statment :===\n', require('mysql2').format(...args),
        '\n===: params :===\n', JSON.stringify(args, null, 4),
        '\n===: mysql query dump end :===\n\n')

      conn && conn.destroy()
      if ([
        'ER_LOCK_DEADLOCK',
        'PROTOCOL_CONNECTION_LOST',
        'ECONNRESET',
        'ETIMEDOUT'
      ].indexOf(e.code) === -1) return Promise.reject(e)
// fix if LOCK then retry time
      let waitime = 20
      if (e.code==='ER_LOCK_DEADLOCK') {
          console.log('DEADLOCK wait 20 sec retry') 
          waitime= 2000
      } 
      return delay(waitime).then(() => query(...args))
    }).then(result => {
      process.nextTick(() => {
        conn && conn.destroy()
      })
      return result
    })

    /*
    conn.query(...args).catch(e => {
      const code = e.code
      console.log('>> mysql error dump: ', e)

      if (
        code === 'PROTOCOL_CONNECTION_LOST' ||
        code === 'ECONNRESET' ||
        code === 'ETIMEDOUT'
      ) {
        proxy.del(key)
        return query(...args)
      }

      close()
      throw e
    }).then(result => {
      conn.release()
      return result
    }) */
  )

  return {
    'format': getFormat(config),
    'getConnection': getConnection,
    'query': query,
    'close': close
  }
}

/**
 * use config close db or close all db
 * @param  {object} config if has config then close config db, else close all
 */
const close = (config) => config ? proxy.del(hash(config)) : proxy.reset()

module.exports = {
  'connect': connect,
  'close': close
}
