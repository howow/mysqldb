# mysqldb


## Install

```
npm install @hokang/mysqldb
```

## Summary

簡單封裝支援 Promise 的 `mysql` api，提供簡化使用函式。
相關參數請參考 [mysql](https://github.com/mysqljs/mysql#pooling-connections) 和
[mysql2](https://github.com/sidorares/node-mysql2#using-connection-pools)
有關 pool 的說明。

## Example

### 使用 async/await
```
var mysql = require('@hokang/mysqldb');
var config={...};
var db = mysql.connect(config);
var conn = await db.getConnection();
var [rows] = await conn.execute("select 1 as result").catch(e => {
    //if sql statment error
    db.close();
    throw e;
});

conn.release();

console.log('result:', rows);
db.close();
// or mysql.close();
```

### 使用 Promise
```
var mysql = require('@hokang/mysqldb');
var config={...};
var db = mysql.connect(config);
db.query("select 2 as result").then(([rows]) => {
    console.log('result:', rows);
    db.close();
});
```

## connect

傳回一個物件，包含有

- getConnection: 傳回一個連線物件
- close: 關閉連線
- query: 開啟一個連線並執行查詢

等函式

## close

內定關閉所有資料庫物件, 如果傳入 config 則關閉該 config 指定的資料庫。
